﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DataSourceSerializer))]
public class DataSourceSerializerInspector : Editor {

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        if(GUILayout.Button("Serialize")) {
            (target as DataSourceSerializer).SerializeData();
        }
    }

}
