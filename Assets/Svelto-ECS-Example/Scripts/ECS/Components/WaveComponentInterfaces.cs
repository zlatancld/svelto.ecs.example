﻿namespace Svelto.ECS.Example.Survive.Enemies.Wave {
    
    public interface IWaveSpawnDataComponent: IComponent {
        float speedMultiplier { get; set; }
        float normalizedPercentageBigEnemies { get; set; }
        uint enemiesToSpawn { get; set; }
    }

    public interface IWaveEnemiesCounter: IComponent {
        uint killedEnemies { get; set; }
        uint leftToSpawn { get; set; }
    }

}

/*
 * I add the following components (to read the number of left enemies and to tag the wave with a number) to the generic namespace 
 * in order to allow the WaveHUD engine to access to these elements without problems of scope. It takes sense because the HUD needs 
 * generic information (a simple counter to read and an ordinal number as label) that can be also used to define the wave entity.
 */

namespace Svelto.ECS.Example.Survive {
    public interface ICounterReaderComponent: IComponent {
        uint counterValue { get; }
    }

    public interface IOrdinalLabel: IComponent {
        uint ordinalValue { get; set; }
    }
}