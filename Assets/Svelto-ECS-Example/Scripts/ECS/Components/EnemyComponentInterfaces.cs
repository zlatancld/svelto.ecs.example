using System;
using UnityEngine;

namespace Svelto.ECS.Example.Survive.Enemies
{
    public interface IEnemySinkComponent : IComponent
    {
        float sinkAnimSpeed { get; }
    }

    public interface IEnemyAttackDataComponent: IComponent
    {
        int   damage         { get; }
        float attackInterval { get; }
        float timer          { get; set; }
    }

    public interface IEnemyMovementComponent: IComponent
    {
        bool navMeshEnabled {  set; }
        Vector3 navMeshDestination { set; }
        bool setCapsuleAsTrigger { set; }
    }

    public interface IEnemyTriggerComponent: IComponent
    {
        EnemyCollisionData entityInRange { get; }
    }

    public struct EnemyCollisionData
    {
        public int otherEntityID;
        public bool collides;

        public EnemyCollisionData(int otherEntityID, bool collides)
        {
            this.otherEntityID = otherEntityID;
            this.collides = collides;
        }
    }

    public interface IEnemyVFXComponent: IComponent
    {
        Vector3             position { set; }
        DispatchOnSet<bool> play     { get; }
    }

    public interface IEnemyStunnedComponent: IComponent {
        bool isSufferingStunnedEffect { get; set; }      // this is true when the enemy is still stunned and can't move
        float stunnedTimeCollector { get; set; }
        float stunnedTargetTime { get; set; }
    }

    public interface IEnemyPushedComponent: IComponent {
        Vector3 pushVector { set; }                      // set this Vector to apply the push force to the enemy
    }
}