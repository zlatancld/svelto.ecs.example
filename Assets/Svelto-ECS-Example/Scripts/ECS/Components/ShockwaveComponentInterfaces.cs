﻿namespace Svelto.ECS.Example.Survive {

    public interface IShockwaveAttributesComponent : IComponent {
        float influenceRange { get; }
        float forceValue { get; }
        float forceZAngle { get; }  // the angle, in relation with the z-axis, to apply the force toward the top.
        float duration { get; }
        float stunnedEffectDuration { get; } 
    }

    public interface IShockwaveFireComponent : IComponent {
        bool                                    isActive { get; set; }
        UnityEngine.Vector3                     sourcePosition { get; }
        System.Collections.Generic.List<int>    inRangeEntitiesId { get; }
    }

    public interface IShockwaveFXComponent: IComponent {
        bool play { set; }
    }

    public interface IShockwaveAttackComponent {
        int shockwaveEntityId { get; }
    }

    public struct ShockwaveAttackInfo {
        public int sourceEntityId;
    }
    
}