using UnityEngine;

namespace Svelto.ECS.Example.Survive.Player.Gun
{
    public interface IGunAttributesComponent: IComponent
    {
        float   timeBetweenBullets { get; }
        Ray     shootRay           { get; }
        float   range              { get; }
        int     damagePerShot      { get; }
        float   timer              { get; set; }
        Vector3 lastTargetPosition { get; set; }
        int leftBullets { get; set; }
    }

    public interface IGunHitTargetComponent : IComponent
    {
        DispatchOnSet<bool> targetHit { get; }
        DispatchOnSet<bool> emptyShot { get; }
    }

    public interface IGunFXComponent: IComponent
    {
        float   effectsDisplayTime { get; }
        Vector3 lineEndPosition    { set; }
        Vector3 lineStartPosition  { set; }
        bool    lineEnabled        { set; }
        bool    play               { set; }
        bool    lightEnabled       { set; }
        bool    playShotAudio      { set; }
        bool    playEmptyShotAudio { set; }
    }

    public interface IAmmoDataComponent: IComponent {
        int bulletsRecharge { get; }
    }
}

namespace Svelto.ECS.Example.Survive.Player {
    public interface IArmedCharacterComponent : IComponent {
        bool isCurrentlyArmed { get; }
        int weaponEntityId { get; }
    }
}