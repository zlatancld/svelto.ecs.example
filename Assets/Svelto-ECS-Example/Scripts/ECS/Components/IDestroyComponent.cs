namespace Svelto.ECS.Example.Survive.Enemies
{
    public interface IDestroyComponent
    {
        DispatchOnChange<bool> destroyed { get; }
    }
}

namespace Svelto.ECS.Example.Survive {

    public interface IDestroyComponent {
        DispatchOnChange<bool> destroyed { get; }
    }

    
}