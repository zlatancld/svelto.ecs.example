﻿using UnityEngine;
using Svelto.ECS.Example.Survive;

[ExecuteInEditMode]
/// <summary>
/// This monobehaviour acts as an utility to generate data source. It's defined through the scene and then serialized in a json file.
/// </summary>
public class DataSourceSerializer : MonoBehaviour {

    private const string SPAWNABLE_OBJECTS_PATH = "/SpawnableObjectsData.json";

    private static bool autoSerialized = false;

    [SerializeField]
    private JsonRandomObjectSpawnData[] spawnableObjects;
        
    void Awake() {
        if(!autoSerialized)
            SerializeData();
    }

    public void SerializeData() {
        autoSerialized = true;

        var jsonStr = JsonHelper.arrayToJson(spawnableObjects);

        System.IO.File.WriteAllText(string.Concat(Application.persistentDataPath, SPAWNABLE_OBJECTS_PATH), jsonStr);

        Utility.Console.Log("Serializing data" + jsonStr);
    }
}
