﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace Svelto.ECS.Example.Survive {

    public class JsonDataSourceProvider : ISourceDataProvider {

        private const string SPAWNABLE_OBJECTS_PATH = "/SpawnableObjectsData.json";

        public IRandomObjectSpawnData[] LoadRandomObjectsSpawnData() {
            string json = File.ReadAllText(string.Concat(Application.persistentDataPath, SPAWNABLE_OBJECTS_PATH));
            IRandomObjectSpawnData[] randomObjectsData = JsonHelper.getJsonArray<JsonRandomObjectSpawnData>(json);
            return randomObjectsData;
        }

    }

}