﻿using UnityEngine;

namespace Svelto.ECS.Example.Survive {

    [System.Serializable]
    public class JsonRandomObjectSpawnData : IRandomObjectSpawnData {
        [SerializeField]
        private GameObject _objectPrefab;
        [SerializeField]
        private float 
            _minRandomTime = 5f,
            _maxRandomTime = 10f;

        public GameObject objectPrefab { get { return _objectPrefab; } }
        public float minRandomTime { get { return _minRandomTime; } }
        public float maxRandomTime { get { return _maxRandomTime; } }
    }
}
