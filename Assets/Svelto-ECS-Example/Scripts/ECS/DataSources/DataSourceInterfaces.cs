﻿using UnityEngine;

namespace Svelto.ECS.Example.Survive {

    public interface ISourceDataProvider {
        IRandomObjectSpawnData[] LoadRandomObjectsSpawnData();
    }
    
    public interface IRandomObjectSpawnData {
        GameObject objectPrefab { get; }
        float minRandomTime { get; }
        float maxRandomTime { get; }
    }

}
