﻿namespace Svelto.ECS.Example.Survive.Enemies.Wave {

    public class WaveCycleStateEntityView : EntityView {
        public IWaveSpawnDataComponent spawnDataComponent;
        public IWaveEnemiesCounter enemiesCounter;
        public IOrdinalLabel waveNumberTagComponent;
    }
    
}

namespace Svelto.ECS.Example.Survive.HUD {

    /// <summary>
    /// This EntityView is part of the Entity wave, used by the WaveHUDEngine to read information about the wave and to update the HUD.
    /// </summary>
    public class HUDWaveInfoEntityView : EntityView {
        public ICounterReaderComponent leftEnemiesToKillComponent;
        public IOrdinalLabel waveNumberTagComponent;
    }

}