﻿namespace Svelto.ECS.Example.Survive {

    public class HealingPackEntityView : EntityView {

        public IHealingPackDataComponent healingPackDataComponent;
        public IPickableComponent pickableComponent;
        public IDestroyComponent recycleComponent;

    }

}