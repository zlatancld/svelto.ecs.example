﻿namespace Svelto.ECS.Example.Survive.Player.Gun {

    public class PickableAmmoEntityView : EntityView {
        public IPickableComponent pickableComponent;
        public IDestroyComponent destroyComponent;
        public IAmmoDataComponent ammoDataComponent;
    }

}