﻿namespace Svelto.ECS.Example.Survive {

    public class ShockwaveSourceEntityView : EntityView {
        public IShockwaveAttributesComponent shockwaveAttributesComponent;
        public IShockwaveFireComponent shockwaveFireComponent;    
    }

}

namespace Svelto.ECS.Example.Survive.Player {

    public class PlayerShockwaveEntityView : EntityView {
        public IShockwaveAttributesComponent attributeComponent;
        public IShockwaveFireComponent fireComponent;
        public IShockwaveFXComponent fxComponent;
    }

}