﻿namespace Svelto.ECS.Example.Survive.HUD {

    /// <summary>
    /// This EntityView represents the part of the HUD that the WaveHUDEngine interacts with.
    /// </summary>
    public class HUDWaveEntityView : EntityView {
        public IAnimationComponent animationComponent;
        public IWaveLeftEnemiesLabelComponent waveLeftEnemiesComponent;
        public IWaveNumLabelComponent waveNumLabelComponent;
    }

}
