﻿using UnityEngine;

namespace Svelto.ECS.Example.Survive.HUD {
    public class BulletsHUDImplementor : MonoBehaviour, IImplementor, IAmmoHUDComponent {

        private UnityEngine.UI.Text _leftBulletTxt;

        public int leftBullets { set { _leftBulletTxt.text = value.ToString(); } }

        // Use this for initialization
        void Awake() {
            _leftBulletTxt = GetComponent<UnityEngine.UI.Text>();
        }
        
    }
}