﻿using UnityEngine;
using Svelto.ECS.Example.Survive.HUD;

namespace Svelto.ECS.Example.Survive.Implementor.HUD {
    public class NewWaveHUDImplementor : MonoBehaviour, IImplementor, IWaveNumLabelComponent {

        private const string NEW_WAVE_FORMAT = "Wave {0}";

        private UnityEngine.UI.Text _newWaveTxt;

        private void Awake() {
            _newWaveTxt = GetComponent<UnityEngine.UI.Text>();
        }

        public uint waveNumber { set { _newWaveTxt.text = string.Format(NEW_WAVE_FORMAT, value); } }

    }
}