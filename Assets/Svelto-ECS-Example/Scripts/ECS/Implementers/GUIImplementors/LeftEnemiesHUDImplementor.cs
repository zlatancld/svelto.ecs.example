﻿using UnityEngine;
using Svelto.ECS.Example.Survive.HUD;

namespace Svelto.ECS.Example.Survive.Implementors.HUD {
    public class LeftEnemiesHUDImplementor : MonoBehaviour, IImplementor, IWaveLeftEnemiesLabelComponent {

        private const string LEFT_ENEMIES_FORMAT = "left to kill: {0}";

        private UnityEngine.UI.Text _leftEnemiesTxt;

        void Awake() {
            _leftEnemiesTxt = GetComponent<UnityEngine.UI.Text>();
        }

        public uint leftEnemiesToKill { set { _leftEnemiesTxt.text = string.Format(LEFT_ENEMIES_FORMAT, value); } }

    }
}