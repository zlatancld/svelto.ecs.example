﻿namespace Svelto.ECS.Example.Survive.Enemies.Wave {
    public class WaveStateImplementor : IImplementor, IWaveSpawnDataComponent, IWaveEnemiesCounter, ICounterReaderComponent, IOrdinalLabel {

        // parameters to implement the components
        private uint _waveNumber, _totalEnemies, _leftEnemiesToSpawn, _currentKilledEnemies;
        private float _bigEnemiesNormPerc, _enemySpeedMultiplier;

        public WaveStateImplementor() {
            _waveNumber = 0;
            _leftEnemiesToSpawn = _currentKilledEnemies = 0;
            _totalEnemies = 10;             // the starting number of enemies to spawn, used for the first wave and then increased
            _bigEnemiesNormPerc = 0.05f;    // the big enemies start with a 5% of probability to be spawned during the first wave
            _enemySpeedMultiplier = 1.0f;   // the multiplier to increase the base speed of each enemy
        }

        public float speedMultiplier { get { return _enemySpeedMultiplier; } set { _enemySpeedMultiplier = value; } }
        public float normalizedPercentageBigEnemies { get { return _bigEnemiesNormPerc; } set { _bigEnemiesNormPerc = value; } }
        public uint enemiesToSpawn { get { return _totalEnemies; } set { _totalEnemies = value; } }
        public uint killedEnemies { get { return _currentKilledEnemies; } set { _currentKilledEnemies = value; } }
        public uint leftToSpawn { get { return _leftEnemiesToSpawn; } set { _leftEnemiesToSpawn = value; } }
        public uint counterValue { get { return _totalEnemies - _currentKilledEnemies; } }
        public uint ordinalValue { get { return _waveNumber; } set { _waveNumber = value; } }
    }
}