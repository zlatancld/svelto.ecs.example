﻿namespace Svelto.ECS.Example.Survive.Enemies {

    public class EnemyStunnedImplementor : IImplementor, IEnemyStunnedComponent {
        public bool isSufferingStunnedEffect { get; set; }
        public float stunnedTimeCollector { get; set; }
        public float stunnedTargetTime { get; set; }
    }

}