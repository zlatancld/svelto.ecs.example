﻿using System.Collections.Generic;
using UnityEngine;

namespace Svelto.ECS.Example.Survive {
    public class ShockwaveTriggerImplementor : MonoBehaviour, IImplementor, IShockwaveFireComponent {

        private Transform _transform;
        private SphereCollider _sphereCollider;
        private List<int> _detectedEntitiesId;

        public bool isActive { get { return _sphereCollider.enabled; } set { _sphereCollider.enabled = value; } }
        public Vector3 sourcePosition { get { return _transform.position; } }
        public List<int> inRangeEntitiesId { get { return _detectedEntitiesId; } }

        // Use this for initialization
        void Awake() {
            _sphereCollider = GetComponent<SphereCollider>();
            _transform = this.transform;
            _detectedEntitiesId = new List<int>();
        }
        
        void OnTriggerEnter(Collider other) {
            _detectedEntitiesId.Add(other.gameObject.GetInstanceID());
        }

        void OnTriggerExit(Collider other) {
            _detectedEntitiesId.Remove(other.gameObject.GetInstanceID());
        }

    }
}