﻿using UnityEngine;

namespace Svelto.ECS.Example.Survive.Player {
    public class ShockwavePlayerImplementor : MonoBehaviour, IImplementor, IShockwaveAttributesComponent, IShockwaveFXComponent {

        [SerializeField]
        private float
            ForceValue = 10f,
            ForceZAngle = 20f,
            Duration = 1,
            StunnedDuration = 2;

        private Transform _transform;
        private MeshRenderer _mesh;

        public float influenceRange { get { return _transform.localScale.x / 2f; } }
        public float forceValue { get { return ForceValue; } }
        public float forceZAngle { get { return ForceZAngle; } }
        public float duration { get { return Duration; } }
        public float stunnedEffectDuration { get { return StunnedDuration; } }

        public bool play { set { _mesh.enabled = value; } }

        // Use this for initialization
        void Awake() {
            _transform = GetComponent<Transform>();
            _mesh = GetComponent<MeshRenderer>();
        }
        
    }
}