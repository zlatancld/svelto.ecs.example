namespace Svelto.ECS.Example.Survive.Player
{
    public class PlayerHealthImplementor : IImplementor, IHealthComponent
    {

        private int _currentHealth;
        private readonly int _maxHealth;

        public int currentHealth { get { return _currentHealth; } set { _currentHealth = UnityEngine.Mathf.Min(_maxHealth, value); } }

        public PlayerHealthImplementor(int startingHealth, int maxHealth)
        {
            _maxHealth = maxHealth;

            // Set the initial health of the player.
            currentHealth = startingHealth;
        }
    }
}