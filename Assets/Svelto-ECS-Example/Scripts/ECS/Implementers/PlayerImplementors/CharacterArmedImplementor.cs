﻿namespace Svelto.ECS.Example.Survive.Player {

    public class CharacterArmedImplementor : IImplementor, IArmedCharacterComponent, IShockwaveAttackComponent {

        // it holds the information about the weapon entity 
        private int _weaponEntityId;
        private bool _isCharacterArmed;      // potentially, this can be set to false if the character loses the weapon (or he can start without one)
        private int _shockwaveEntityId;

        public bool isCurrentlyArmed { get { return _isCharacterArmed; } }
        public int weaponEntityId { get { return _weaponEntityId; } }
        public int shockwaveEntityId { get { return _shockwaveEntityId; } }

        /// <summary>
        /// Use this constructor if the character already starts with a weapon.
        /// </summary>
        public CharacterArmedImplementor(int existingWeaponId, int shockwaveEntityId) {
            _weaponEntityId = existingWeaponId;
            _shockwaveEntityId = shockwaveEntityId;
            _isCharacterArmed = true;
        }

    }

}