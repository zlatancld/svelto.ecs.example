﻿using UnityEngine;

namespace Svelto.ECS.Example.Survive.Player.Gun {
    public class AmmoImplementor : MonoBehaviour, IImplementor, IAmmoDataComponent {

        [SerializeField]
        private int bullets;        // the number of bullets this ammo pack provides

        public int bulletsRecharge { get { return bullets; } }

    }
}