﻿using UnityEngine;

namespace Svelto.ECS.Example.Survive.Implementors {
    public class HealingPackImplementor : MonoBehaviour, IImplementor, IHealingPackDataComponent {

        [SerializeField]
        private int healthRefillAmount;

        public int healingValue { get { return healthRefillAmount; } }

    }
}
