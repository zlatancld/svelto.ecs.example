﻿using UnityEngine;

namespace Svelto.ECS.Example.Survive.Implementors {
    public class PickableImplementor : MonoBehaviour, IImplementor, IPickableComponent, IDestroyComponent {

        private DispatchOnSet<bool> _isPicked;
        private DispatchOnChange<bool> _destroyed;
        private int _pickerId;

        public DispatchOnSet<bool> isPicked { get { return _isPicked; } }
        public DispatchOnChange<bool> destroyed { get { return _destroyed; } }
        public int pickerEntityId { get { return _pickerId; } }

        // Use this for initialization
        void Awake() {
            _isPicked = new DispatchOnSet<bool>(gameObject.GetInstanceID());
            _destroyed = new DispatchOnChange<bool>(gameObject.GetInstanceID());
            _destroyed.NotifyOnValueSet(OnDestroyObject);
        }

        void OnTriggerEnter(Collider other) {
            // set the picked id and change the 'picked' status
            _pickerId = other.gameObject.GetInstanceID();
            _isPicked.value = true;
        }

        void OnDestroyObject(int senderId, bool destroy) {
            if(destroy)
                Destroy(gameObject);
        }
    }
}