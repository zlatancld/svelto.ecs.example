﻿using System.Collections;
using Svelto.Tasks;
using Svelto.Tasks.Enumerators;

namespace Svelto.ECS.Example.Survive.Player {

    public class PlayerShockwaveEngine : SingleEntityViewEngine<PlayerEntityView>, IQueryingEntityViewEngine {

        private PlayerEntityView _playerEntity;
        private PlayerShockwaveEntityView _shockwaveEntity;
        private readonly ITaskRoutine _taskRoutine;
        private readonly ISequencer _attackSequence;
        private readonly WaitForSecondsEnumerator _waitForSecs;

        public IEntityViewsDB entityViewsDB { get; set; }

        public void Ready() {
        }

        public PlayerShockwaveEngine(ISequencer attackSequence) {
            _attackSequence = attackSequence;
            _waitForSecs = new WaitForSecondsEnumerator(0f);
            _taskRoutine = TaskRunner.Instance.AllocateNewTaskRoutine().SetEnumerator(Tick()).SetScheduler(StandardSchedulers.updateScheduler);
        }

        protected override void Add(PlayerEntityView entityView) {
            _playerEntity = entityView;
            _shockwaveEntity = entityViewsDB.QueryEntityView<PlayerShockwaveEntityView>(
                _playerEntity.shockwaveAttackComponent.shockwaveEntityId);

            _taskRoutine.Start();
        }

        protected override void Remove(PlayerEntityView entityView) {
            _taskRoutine.Stop();
            _playerEntity = null;
            _shockwaveEntity = null;
        }

        private void DeactivateShockwave() {
            if(_shockwaveEntity != null) {
                _shockwaveEntity.fireComponent.isActive = false;
                _shockwaveEntity.fxComponent.play = false;
            }
        }

        IEnumerator Tick() {
            // be sure the shockwave is deactivated
            DeactivateShockwave();

            while(true) {
                if(_playerEntity != null) {
                    if(!_shockwaveEntity.fireComponent.isActive) {
                        // check if the player launch a new wave
                        if(_playerEntity.inputComponent.specialFire) {
                            // activate the shockwave
                            _shockwaveEntity.fireComponent.isActive = true;
                            _shockwaveEntity.fxComponent.play = true;

                            ShockwaveAttackInfo info = new ShockwaveAttackInfo();
                            info.sourceEntityId = _playerEntity.shockwaveAttackComponent.shockwaveEntityId;
                            _attackSequence.Next(this, ref info);

                            // wait to deactivate the shockwave after its duration
                            _waitForSecs.Reset(_shockwaveEntity.attributeComponent.duration);
                            yield return _waitForSecs;

                            DeactivateShockwave();
                        }
                    }
                }

                yield return null;
            }
        }
    }

}