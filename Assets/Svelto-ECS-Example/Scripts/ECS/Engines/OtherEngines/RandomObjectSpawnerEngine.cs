﻿using System.Collections;
using UnityEngine;
using Svelto.Tasks.Enumerators;
using Svelto.Tasks;

namespace Svelto.ECS.Example.Survive {
    public class RandomObjectSpawnerEngine : IEngine {
        
        private Factories.IGameObjectFactory _goFactory;
        private IEntityFactory _entityFactory;
        private IRayCaster _raycaster;
        private ITaskRoutine[] _myRoutines;     // in case if the engine wants to stop or resume the routines

        // parameters to detect if the random raycast hits the floor
        private const float SPAWN_Y_QUOTE = 0.5f;
        // for the sake of semplicity, the system knows the space dimensions where it tries to spawn objects into.
        private const float RAY_CIRCLE_SPACE = 25f;
        private static readonly int FLOOR_MASK = LayerMask.GetMask("Floor");

        public RandomObjectSpawnerEngine(Factories.IGameObjectFactory goFactory, IEntityFactory entityFactory,
            IRayCaster raycaster, ISourceDataProvider sourceDataProvider) {
            
            _goFactory = goFactory;
            _entityFactory = entityFactory;
            _raycaster = raycaster;

            // create a task for each type of object data to spawn
            var sourceDataArr = sourceDataProvider.LoadRandomObjectsSpawnData();
            _myRoutines = new ITaskRoutine[sourceDataArr.Length];

            for(int i = 0; i < sourceDataArr.Length; i++) {
                _myRoutines[i] = TaskRunner.Instance.AllocateNewTaskRoutine().SetEnumerator(SpawningLoop(sourceDataArr[i]));
                _myRoutines[i].Start();
            }
        }

        public void Ready() { }

        IEnumerator SpawningLoop(IRandomObjectSpawnData sourceData) {
            WaitForSecondsEnumerator waitForSecs = new WaitForSecondsEnumerator(0);

            while(true) {
                // select a random time between min a max provided by the source data
                float randomWaitSecs = Random.Range(sourceData.minRandomTime, sourceData.maxRandomTime);
                waitForSecs.Reset(randomWaitSecs);

                yield return waitForSecs;

                SpawnRandomObject(sourceData.objectPrefab);
            }
        }
        
        private Vector3 SelectRandomEmptyPosition() {
            var raycasterResult = -1;

            // the starting point for the raycast is a bit over the floor
            Vector3 result = Vector3.zero;
            result.y = SPAWN_Y_QUOTE;
            Vector3 hitPoint;

            while(raycasterResult == -1) {
                // choose a random point 
                Vector2 random2dPoint = Random.insideUnitCircle * RAY_CIRCLE_SPACE;
                result.x = random2dPoint.x;
                result.z = random2dPoint.y;

                Ray ray = new Ray(result, Vector3.down);

                raycasterResult = _raycaster.CheckHit(ray, SPAWN_Y_QUOTE + 1, FLOOR_MASK, out hitPoint);
            }

            // the raycaster has hit the floor
            return result;
        }

        /// <summary>
        /// This method spawns a new random object in a random position.
        /// </summary>
        private void SpawnRandomObject(GameObject objectPrefab) {
            GameObject spawnedGO = _goFactory.Build(objectPrefab);

            // force the y quote to be equal the prefab one
            Vector3 randomSpawningPosition = SelectRandomEmptyPosition();
            randomSpawningPosition.y = objectPrefab.transform.position.y;
            
            spawnedGO.transform.position = randomSpawningPosition;

            // create the entity retrieving the descriptor holder (in this case is used to have a polymorphic process of spawning)
            var entityDescriptor = spawnedGO.GetComponent<IEntityDescriptorHolder>().RetrieveDescriptor();
            _entityFactory.BuildEntity(spawnedGO.GetInstanceID(), entityDescriptor, spawnedGO.GetComponentsInChildren<IImplementor>());
        }

    }
}