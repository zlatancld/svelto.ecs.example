﻿namespace Svelto.ECS.Example.Survive {

    public class HealingPackPickedEngine : SingleEntityViewEngine<HealingPackEntityView>, IQueryingEntityViewEngine {

        private ISequencer _healingSequence;
        private IEntityFunctions _entityFunctions;
        public IEntityViewsDB entityViewsDB { set; private get; }

        public HealingPackPickedEngine(ISequencer healingSequence, IEntityFunctions entityFunctions) {
            _healingSequence = healingSequence;
            _entityFunctions = entityFunctions;
        }

        public void Ready() { }

        protected override void Add(HealingPackEntityView entityView) {
            // just add the notify method to the added healing pack
            entityView.pickableComponent.isPicked.NotifyOnValueSet(OnPackPicked);
        }

        protected override void Remove(HealingPackEntityView entityView) {
            // remove the notify method
            entityView.pickableComponent.isPicked.StopNotify(OnPackPicked);
        }

        private void OnPackPicked(int sender, bool isPicked) {
            if(isPicked) {
                // retrieve the HealingPackEntityView to get the picker info
                HealingPackEntityView healingPack = entityViewsDB.QueryEntityView<HealingPackEntityView>(sender);
                if(healingPack == null) {
                    UnityEngine.Debug.LogError("The healing pack is null, impossible to proceed");
                    return;
                }

                // create the healing info pack to continue the healing process
                HealingInfo hi = new HealingInfo(
                    healingPack.healingPackDataComponent.healingValue,
                    healingPack.pickableComponent.pickerEntityId);

                _healingSequence.Next(this, ref hi);

                // the healing pack can be destroyed now
                healingPack.recycleComponent.destroyed.value = true;

                // remove the entity
                _entityFunctions.RemoveEntity<HealingPackDescriptor>(sender);
            }
        }

    }

}
