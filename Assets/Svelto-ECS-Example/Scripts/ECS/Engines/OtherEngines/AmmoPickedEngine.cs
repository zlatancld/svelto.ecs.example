﻿namespace Svelto.ECS.Example.Survive.Player.Gun {

    public class AmmoPickedEngine : SingleEntityViewEngine<PickableAmmoEntityView>, IQueryingEntityViewEngine {

        private readonly IEntityFunctions _entityFuncs;
        private readonly ISequencer _updateAmmoSequencer;

        public IEntityViewsDB entityViewsDB { set; private get; }

        public AmmoPickedEngine(ISequencer updateAmmoSequencer, IEntityFunctions entityFuncs) {
            _entityFuncs = entityFuncs;
            _updateAmmoSequencer = updateAmmoSequencer;
        }

        public void Ready() { }

        protected override void Add(PickableAmmoEntityView entityView) {
            entityView.pickableComponent.isPicked.NotifyOnValueSet(OnAmmoPicked);
        }

        protected override void Remove(PickableAmmoEntityView entityView) {
        }
        
        private void OnAmmoPicked(int senderID, bool isPicked) {
            if(isPicked) {
                // retrieve the picked ammo entity
                var ammoEntity = entityViewsDB.QueryEntityView<PickableAmmoEntityView>(senderID);

                // retrieve the player who picked ammo
                var playerEntity = entityViewsDB.QueryEntityView<PlayerEntityView>(ammoEntity.pickableComponent.pickerEntityId);

                // check if he's armed and recharge the weapon
                if(playerEntity.armedComponent.isCurrentlyArmed) {
                    // retrieve the gun using its entity id
                    var gunEntity = entityViewsDB.QueryEntityView<GunEntityView>(playerEntity.armedComponent.weaponEntityId);
                    gunEntity.gunComponent.leftBullets += ammoEntity.ammoDataComponent.bulletsRecharge;

                    int leftBullets = gunEntity.gunComponent.leftBullets;
                    _updateAmmoSequencer.Next(this, ref leftBullets);

                    // the object can be destroyed
                    ammoEntity.destroyComponent.destroyed.value = true;
                    _entityFuncs.RemoveEntity<AmmoDescriptor>(ammoEntity.ID);
                }
            }
        }

    }
}