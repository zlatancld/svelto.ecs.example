﻿namespace Svelto.ECS.Example.Survive.HUD {
    public class HUDAmmoEngine : SingleEntityViewEngine<HUDAmmoEntityView>, IStep<int> {

        private HUDAmmoEntityView _ammoHUD;

        protected override void Add(HUDAmmoEntityView entityView) {
            _ammoHUD = entityView;
        }

        protected override void Remove(HUDAmmoEntityView entityView) {
            _ammoHUD = null;
        }

        void UpdateAmmo(int leftBullets) {
            if(_ammoHUD != null)
                _ammoHUD.ammoHUDComponent.leftBullets = leftBullets;
        }

        public void Step(ref int leftBullets, int condition) {
            UpdateAmmo(leftBullets);
        }

    }
}