﻿namespace Svelto.ECS.Example.Survive.HUD {

    public class HUDWaveEngine : MultiEntityViewsEngine<HUDWaveEntityView, HUDWaveInfoEntityView>, IStep<DamageInfo>, IStep<uint>{

        private const string NEW_WAVE_TRIGGER = "NewWave";

        private HUDWaveEntityView _hudWaveEV;           // the HUD component linked to the wave
        private HUDWaveInfoEntityView _hudWaveInfoEV;   // the info about the wave entity
        
        protected override void Add(HUDWaveEntityView entityView) {
            _hudWaveEV = entityView;
            UpdateEnemiesToKillLabel();
        }
        protected override void Add(HUDWaveInfoEntityView entityView) {
            _hudWaveInfoEV = entityView;
            UpdateEnemiesToKillLabel();
        }
        protected override void Remove(HUDWaveEntityView entityView) { _hudWaveEV = null; }
        protected override void Remove(HUDWaveInfoEntityView entityView) { _hudWaveInfoEV = null; }

        private void UpdateEnemiesToKillLabel() {
            // update the HUD
            if(_hudWaveEV != null && _hudWaveInfoEV != null) 
                _hudWaveEV.waveLeftEnemiesComponent.leftEnemiesToKill = _hudWaveInfoEV.leftEnemiesToKillComponent.counterValue;
        }

        private void ShowNewWaveFeedback(uint waveNumber) {
            UpdateEnemiesToKillLabel();

            if(_hudWaveEV != null) {
                // update the HUD with the new wave number
                _hudWaveEV.waveNumLabelComponent.waveNumber = waveNumber;

                // launch the animation for a new wave
                _hudWaveEV.animationComponent.trigger = NEW_WAVE_TRIGGER;
            }
        }

        public void Step(ref uint token, int condition) {
            // called when a new wave is launched to show a feedback
            ShowNewWaveFeedback(token);
        }

        public void Step(ref DamageInfo token, int condition) {
            if(condition == DamageCondition.Dead) {
                // a new enemy has been killed, update the HUD label
                UpdateEnemiesToKillLabel();
            }
        }
        
    }

}