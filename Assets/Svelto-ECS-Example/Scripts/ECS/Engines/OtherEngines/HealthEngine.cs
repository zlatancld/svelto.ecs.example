namespace Svelto.ECS.Example.Survive
{
    public class HealthEngine : IQueryingEntityViewEngine, IStep<DamageInfo>, IStep<HealingInfo>
    {
        public void Ready()
        { }

        public HealthEngine(ISequencer damageSequence, ISequencer healingSequence = null)
        {
            _damageSequence = damageSequence;
            _healingSequence = healingSequence;
        }

        public IEntityViewsDB entityViewsDB { set; private get; }

        public void Step(ref DamageInfo damage, int condition)
        {
            var entityView      = entityViewsDB.QueryEntityView<HealthEntityView>(damage.entityDamagedID);
            var healthComponent = entityView.healthComponent;

            healthComponent.currentHealth -= damage.damagePerShot;

            //the HealthEngine can branch the sequencer flow triggering two different
            //conditions
            if (healthComponent.currentHealth <= 0)
                _damageSequence.Next(this, ref damage, DamageCondition.Dead);
            else
                _damageSequence.Next(this, ref damage, DamageCondition.Damage);
        }

        public void Step(ref HealingInfo healing, int condition) {
            var entityView = entityViewsDB.QueryEntityView<HealthEntityView>(healing.treatedEntityId);
            var healthComp = entityView.healthComponent;

            healthComp.currentHealth += healing.healingValue;

            _healingSequence.Next(this, ref healing);
        }

        readonly ISequencer  _damageSequence;
        readonly ISequencer _healingSequence;
    }
}
