﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Svelto.ECS.Example.Survive.Enemies {

    /// <summary>
    /// This engine wait for the message about an active shockwave and check if the enemies should be pushed away.
    /// </summary>
    public class EnemyPushEngine : IEngine, IQueryingEntityViewEngine, IStep<ShockwaveAttackInfo> {

        // to avoid the loop over all the enemies, cache the pushed ones in a list
        private readonly List<int> _cachedPushedEnemiesId;
        private readonly ITime _time;

        private const float DRAG_PUSHED_ENEMIES = 0.5f;

        public IEntityViewsDB entityViewsDB { set; get; }

        public EnemyPushEngine(ITime time) {
            _cachedPushedEnemiesId = new List<int>();
            _time = time;

            Tick().Run();
        }

        public void Ready() {
        }

        IEnumerator Tick() {
            while(true) {
                // check if there are pushed enemies to unlock
                for(int i = _cachedPushedEnemiesId.Count - 1; i >= 0; i--) {
                    // query to check if the enemy still 
                    EnemyEntityView entityView;
                    bool exist = entityViewsDB.TryQueryEntityView(_cachedPushedEnemiesId[i], out entityView);

                    if(exist) {
                        entityView.stunnedComponent.stunnedTimeCollector += _time.deltaTime;
                        if(entityView.stunnedComponent.stunnedTimeCollector >= entityView.stunnedComponent.stunnedTargetTime) {
                            // unlock the enemy and remove it from the list
                            entityView.stunnedComponent.isSufferingStunnedEffect = false;
                            entityView.rigidBodyComponent.drag = Mathf.Infinity;
                            entityView.movementComponent.navMeshEnabled = true;

                            _cachedPushedEnemiesId.RemoveAt(i);
                        }
                    }
                    else {
                        _cachedPushedEnemiesId.RemoveAt(i);
                    }
                } 
                yield return null;
            }
        }

        private void ApplyPushOverEnemy(EnemyEntityView enemy, ShockwaveSourceEntityView shockwave) {
            // calculate the push force
            Vector3 sourceToEnemyDir = enemy.positionComponent.position - shockwave.shockwaveFireComponent.sourcePosition;
            sourceToEnemyDir.Normalize();
            sourceToEnemyDir.y = 0;
            // rotate the force vector toward the top for a better effect
            sourceToEnemyDir = Quaternion.AngleAxis(shockwave.shockwaveAttributesComponent.forceZAngle, 
                Vector3.forward * Mathf.Sign(sourceToEnemyDir.x)) * sourceToEnemyDir;

            enemy.movementComponent.navMeshEnabled = false;
            enemy.rigidBodyComponent.drag = DRAG_PUSHED_ENEMIES;
            enemy.stunnedComponent.isSufferingStunnedEffect = true;
            enemy.stunnedComponent.stunnedTargetTime = shockwave.shockwaveAttributesComponent.stunnedEffectDuration;
            enemy.stunnedComponent.stunnedTimeCollector = 0f;

            Vector3 force = sourceToEnemyDir * shockwave.shockwaveAttributesComponent.forceValue;
            enemy.pushedComponent.pushVector = force;

            // add to the list of observed enemies
            _cachedPushedEnemiesId.Add(enemy.ID);
        }

        IEnumerator CheckShockwaveApplication(ShockwaveSourceEntityView shockwave) {
            var shockwaveComp = shockwave.shockwaveFireComponent;
            while(shockwaveComp.isActive) {
                // loop over the enemies in the range and check if they must be pushed away
                for(int i = shockwaveComp.inRangeEntitiesId.Count - 1; i >= 0; i--) {
                    var entityId = shockwaveComp.inRangeEntitiesId[i];

                    // be sure the entity is an enemy
                    EnemyEntityView enemyEntity;
                    if(entityViewsDB.TryQueryEntityView(entityId, out enemyEntity)){
                        if(!enemyEntity.stunnedComponent.isSufferingStunnedEffect)
                            ApplyPushOverEnemy(enemyEntity, shockwave);

                        // remove the enemy from the list to reduce the calculation on the next frame
                        shockwaveComp.inRangeEntitiesId.RemoveAt(i);
                    }
                }

                yield return null;
            }
        }

        public void Step(ref ShockwaveAttackInfo token, int condition) {
            // retrieve the shock wave
            var shockwaveSource = entityViewsDB.QueryEntityView<ShockwaveSourceEntityView>(token.sourceEntityId);
            // launch the routine for a shock wave
            CheckShockwaveApplication(shockwaveSource).Run();
        }
        
    }
}