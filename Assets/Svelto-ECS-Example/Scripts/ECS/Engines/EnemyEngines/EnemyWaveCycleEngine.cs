﻿using Svelto.Tasks;
using Svelto.Tasks.Enumerators;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Svelto.ECS.Example.Survive.Enemies.Wave {

    public class EnemyWaveCycleEngine: SingleEntityViewEngine<WaveCycleStateEntityView>, IStep<DamageInfo> {

        private const float SPAWN_WAIT_TIME = 1f;
        private const float REST_TIME_BETWEEN_WAVES = 4f;

        // The constant values to increase the difficulty of the wave for each aspect.
        private const float WAVE_ENEMIES_COUNT_MUL = 1.25f;
        private const float WAVE_BIG_ENEMIES_MUL = 1.2f;
        private const float WAVE_ENEMIES_SPEED_MUL = 1.3f;

        private readonly ITaskRoutine _taskRoutine;
        private readonly Factories.IGameObjectFactory _gameobjectFactory;
        private readonly IEntityFactory _entityFactory;
        private readonly ISequencer _enemyDamageSequencer, _newWaveSequencer;
        private readonly WaitForSecondsEnumerator
            _waitForSecondsEnumTick = new WaitForSecondsEnumerator(SPAWN_WAIT_TIME),
            _waitForSecondsEnumLaunchWave = new WaitForSecondsEnumerator(REST_TIME_BETWEEN_WAVES);

        private WaveCycleStateEntityView _cachedWaveState;

        public EnemyWaveCycleEngine(Factories.IGameObjectFactory goFactory, IEntityFactory entityFactory,
            ISequencer enemyDamageSequencer, ISequencer newWaveSequencer) {

            _taskRoutine = TaskRunner.Instance.AllocateNewTaskRoutine().SetEnumerator(IntervaledTick());
            _enemyDamageSequencer = enemyDamageSequencer;
            _newWaveSequencer = newWaveSequencer;
            _entityFactory = entityFactory;
            _gameobjectFactory = goFactory;
        }

        protected override void Add(WaveCycleStateEntityView entityView) {
            _cachedWaveState = entityView;

            // launch the cycle to define the waves behaviour
            _taskRoutine.Start();
        }

        protected override void Remove(WaveCycleStateEntityView entityView) {
            _taskRoutine.Stop();
            _cachedWaveState = null;
        }

        IEnumerator IntervaledTick() {
                
            var enemiesToSpawn = ReadEnemySpawningDataServiceRequest();

            // First initialize the wave, launch it for the first time
            LaunchNewWave();

            while(true) {
                
                // Wait and check to spawn a new enemy every SPAWN_WAIT_TIME seconds.              
                yield return _waitForSecondsEnumTick;

                if(enemiesToSpawn != null && 
                    _cachedWaveState.enemiesCounter.leftToSpawn > 0) {

                    // The next enemy can be spawned. 
                    // Select a random enemy but using the percentage of 'big enemy spawning'.
                    bool spawnBigEnemy = Random.Range(0f, 1f) <= 
                        _cachedWaveState.spawnDataComponent.normalizedPercentageBigEnemies;

                    // For semplicity, I assume to know the big enemy is only placed in the last position of the enemiesToSpawn array.
                    int enemyToSpawnIndex = spawnBigEnemy ? enemiesToSpawn.Length - 1 : 
                        Random.Range(0, enemiesToSpawn.Length - 1);
                    
                    SpawnNewEnemy(enemiesToSpawn[enemyToSpawnIndex]);
                }
            }
        }

        private void SpawnNewEnemy(JSonEnemySpawnData enemyType) {
            // Find a random index between zero and one less than the number of spawn points.
            int spawnPointIndex = Random.Range(0, enemyType.spawnPoints.Length);
            var spawnInfo = enemyType.spawnPoints[spawnPointIndex];

            // Create an instance of the enemy prefab at the randomly selected spawn point position and rotation.
            var go = _gameobjectFactory.Build(enemyType.enemyPrefab);
            var data = go.GetComponent<EnemyAttackDataHolder>();

            // Create the list of implementors to build the entity.
            List<IImplementor> implementors = new List<IImplementor>();
            go.GetComponentsInChildren(implementors);
            implementors.Add(new EnemyAttackImplementor(data.timeBetweenAttacks, data.attackDamage));
            implementors.Add(new EnemyStunnedImplementor());
            
            _entityFactory.BuildEntity<EnemyEntityDescriptor>(go.GetInstanceID(), implementors.ToArray());

            // initialize the starting position and rotation.
            go.transform.position = spawnInfo.position;
            go.transform.rotation = spawnInfo.rotation;
            // here I need something to access the created EnemyEntityView, to set the speed multiplier (and also position and rotation) with the 
            // same level of abstraction provided by the components (simply I don't want to access the nav mesh directly to set the speed here). 
            // My current solution is to get the component manually using the GetComponent.
            go.GetComponent<ISpeedMultiplierComponent>().speedMultiplier = _cachedWaveState.spawnDataComponent.speedMultiplier;

            // decrease the number of enemies to spawn
            _cachedWaveState.enemiesCounter.leftToSpawn--;
        }

        /// <summary>
        /// This method is called when a new wave must be launched. It increase the difficulty and set the new parameters to be used
        /// during the normal wave lifecycle.
        /// </summary>
        private void LaunchNewWave() {
            // If the current number of the wave is zero, don't apply the multiplier.
            if(_cachedWaveState.waveNumberTagComponent.ordinalValue != 0) {
                // enemies to spawn multiplier
                _cachedWaveState.spawnDataComponent.enemiesToSpawn = (uint)Mathf.RoundToInt(
                    _cachedWaveState.spawnDataComponent.enemiesToSpawn * WAVE_ENEMIES_COUNT_MUL);

                // percentage 'big enemy' multiplier
                _cachedWaveState.spawnDataComponent.normalizedPercentageBigEnemies = Mathf.Min(
                    1f, _cachedWaveState.spawnDataComponent.normalizedPercentageBigEnemies * WAVE_BIG_ENEMIES_MUL);

                // enemies speed multiplier
                _cachedWaveState.spawnDataComponent.speedMultiplier *= WAVE_ENEMIES_SPEED_MUL;
            }
            _cachedWaveState.waveNumberTagComponent.ordinalValue++;

            // reset the wave counters
            _cachedWaveState.enemiesCounter.killedEnemies = 0;
            _cachedWaveState.enemiesCounter.leftToSpawn = _cachedWaveState.spawnDataComponent.enemiesToSpawn;

            // process the next step of the sequence for a new wave, passing the number of the wave (because a parameter is required so
            // I suppose the passage of the wave number is a good approach).
            uint waveNum = _cachedWaveState.waveNumberTagComponent.ordinalValue;
            _newWaveSequencer.Next(this, ref waveNum, 0);
        }

        public void Step(ref DamageInfo token, int condition) {
            if(condition == DamageCondition.Dead) {
                // increase the number of killed enemies
                _cachedWaveState.enemiesCounter.killedEnemies++;

                // continue the sequence flow
                _enemyDamageSequencer.Next(this, ref token, condition);

                // check if a new wave must be launched
                bool newWave = _cachedWaveState.enemiesCounter.killedEnemies >= _cachedWaveState.spawnDataComponent.enemiesToSpawn;
                if(newWave)
                    LaunchWavesAfterRest().Run();
            }
        }

        private IEnumerator LaunchWavesAfterRest() {
            _waitForSecondsEnumLaunchWave.Reset(REST_TIME_BETWEEN_WAVES);
            yield return _waitForSecondsEnumLaunchWave;

            LaunchNewWave();
        }

        private static JSonEnemySpawnData[] ReadEnemySpawningDataServiceRequest() {
            string json = File.ReadAllText(Application.persistentDataPath + "/EnemySpawningData.json");

            JSonEnemySpawnData[] enemiestoSpawn = JsonHelper.getJsonArray<JSonEnemySpawnData>(json);

            return enemiestoSpawn;
        }

    }

}