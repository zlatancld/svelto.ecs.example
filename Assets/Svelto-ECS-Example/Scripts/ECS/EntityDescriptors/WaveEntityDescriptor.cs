﻿using Svelto.ECS.Example.Survive.HUD;

namespace Svelto.ECS.Example.Survive.Enemies.Wave {
    public class WaveEntityDescriptor : GenericEntityDescriptor<WaveCycleStateEntityView, HUDWaveInfoEntityView> {

    }
}