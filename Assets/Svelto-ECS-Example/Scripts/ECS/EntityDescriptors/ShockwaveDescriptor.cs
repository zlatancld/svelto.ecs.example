﻿using Svelto.ECS.Example.Survive.Player;

namespace Svelto.ECS.Example.Survive {

    public class ShockwaveDescriptor : GenericEntityDescriptor<PlayerShockwaveEntityView, ShockwaveSourceEntityView> {
    }

}